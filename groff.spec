%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\([^.]*\\.pl\\)
%{!?with_x:%global with_x 1}
Name: groff
Version: 1.22.4
Release: 10
Summary: A typesetting system
License: GPLv3+ and GFDL and BSD and MIT
URL: http://www.gnu.org/software/groff/
Source: http://ftp.gnu.org/gnu/groff/groff-%{version}.tar.gz

BuildRequires: gcc, gcc-c++ texinfo
BuildRequires: git, netpbm-progs, perl-generators, psutils, ghostscript
BuildRequires: libXaw-devel, libXmu-devel
BuildRequires: jbig2dec-libs 
Requires:      coreutils
Requires:     %{name}-base = %{version}-%{release}

Obsoletes:    %{name}-base < %{version}-%{release}
Obsoletes:    %{name}-perl < %{version}-%{release}
Obsoletes:    %{name}-x11 < %{version}-%{release}
Obsoletes:    %{name}-gxditview < 1.20.1
Provides:     %{name}-perl
Provides:     %{name}-x11
Provides:     groff-gxditview = %{version}-%{release}
Provides:     nroff-i18n = %{version}-%{release}

%description
Groff (GNU troff) is a typesetting system that reads plain text mixed
with formatting commands and produces formatted output. Output may be
PostScript or PDF, html, or ASCII/UTF8 for display at the terminal.
Formatting commands may be either low-level typesetting requests
(“primitives”) or macros from a supplied set. Users may also write
their own macros. All three may be combined.

%package        base
Summary:        Base parts of groff formatting system

%description    base
The groff-base package contains base parts of groff formatting system
which are required to display manual pages.

%package        help
Summary:        Documents for %{name}
Buildarch:      noarch
Requires:       man info
Requires:       groff = %{version}-%{release}

Obsoletes:      %{name}-doc < %{version}-%{release}
Provides:       %{name}-doc

%if %{with_x}
%package x11
Summary: Parts of the groff formatting system that require X Windows System
Requires: groff-base = %{version}-%{release}
BuildRequires: libXaw-devel, libXmu-devel
Provides: groff-gxditview = %{version}-%{release}
Obsoletes: groff-gxditview < 1.20.1
Conflicts: groff < 1.22.4-8

%description x11
The groff-x11 package contains the parts of the groff text processor
package that require X Windows System. These include gxditview (display
groff intermediate output files on X Windows System display) and
xtotroff (converts X font metrics into groff font metrics).
%endif


%description help
Man pages and other related documents for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

for file in NEWS src/devices/grolbp/grolbp.1.man doc/webpage.ms \
                contrib/mm/*.man contrib/mom/examples/{README.txt,*.mom,mom.vim}; do
    iconv -f iso-8859-1 -t utf-8 < "$file" > "${file}_"
    mv "${file}_" "$file"
done

%build
%configure \
    --docdir=%{_docdir}/%{name}-%{version} \
    --with-appresdir=%{_datadir}/X11/app-defaults \
    --with-grofferdir=%{_datadir}/%{name}/%{version}/groffer
%make_build

%install
%make_install

# some binaries need alias with 'g' or 'z' prefix
for file in g{nroff,troff,tbl,pic,eqn,neqn,refer,lookbib,indxbib,soelim} zsoelim; do
    ln -s ${file#?} %{buildroot}%{_bindir}/${file}
    ln -s ${file#?}.1.gz %{buildroot}%{_mandir}/man1/${file}.1.gz
done

# fix absolute symlink to relative symlink
rm -f %{buildroot}%{_docdir}/%{name}-%{version}/pdf/mom-pdf.pdf
ln -s ../examples/mom/mom-pdf.pdf %{buildroot}%{_docdir}/%{name}-%{version}/pdf/mom-pdf.pdf

# rename groff downloadable postscript fonts(bz #477394)
for file in $(find %{buildroot}%{_datadir}/%{name}/%{version}/font/devps -name "*.pfa"); do
    mv ${file} ${file}_
done
sed --in-place 's/\.pfa$/.pfa_/' %{buildroot}%{_datadir}/%{name}/%{version}/font/devps/download

# remove unnecessary files
rm -f %{buildroot}%{_infodir}/dir

# fix privileges
chmod 755 %{buildroot}%{_datadir}/groff/%{version}/groffer/version.sh
chmod 755 %{buildroot}%{_datadir}/groff/%{version}/font/devlj4/generate/special.awk

# remove CreationDate
pushd %{buildroot}%{_docdir}/%{name}-%{version}
    find -name "*.html" | xargs sed -i "/^<!-- CreationDate: /d"
    find -name "*.ps"   | xargs sed -i "/^%%%%CreationDate: /d"
popd

# /bin/sed moved to /usr/bin/sed
sed --in-place 's|#! /bin/sed -f|#! /usr/bin/sed -f|' %{buildroot}%{_datadir}/groff/%{version}/font/devps/generate/symbol.sed

%check
make check

%pre

%preun

%post

%postun

%files
%defattr(-,root,root)
%{_datadir}/%{name}/%{version}/font/devX*/
%{_datadir}/%{name}/%{version}/tmac/X.tmac
%{_datadir}/%{name}/%{version}/tmac/Xps.tmac
%{_datadir}/%{name}/%{version}/font/devcp1047/
%{_datadir}/%{name}/%{version}/font/devdvi/
%{_datadir}/%{name}/%{version}/font/devlbp/
%{_datadir}/%{name}/%{version}/font/devlj4/
%{_datadir}/%{name}/%{version}/font/devpdf/
%{_datadir}/%{name}/%{version}/groffer/
%{_datadir}/%{name}/%{version}/oldfont/
%{_datadir}/%{name}/%{version}/pic/
%{_datadir}/%{name}/%{version}/tmac/62bit.tmac
%{_datadir}/%{name}/%{version}/tmac/a4.tmac
%{_datadir}/%{name}/%{version}/tmac/dvi.tmac
%{_datadir}/%{name}/%{version}/tmac/e.tmac
%{_datadir}/%{name}/%{version}/tmac/ec.tmac
%{_datadir}/%{name}/%{version}/tmac/hdmisc.tmac
%{_datadir}/%{name}/%{version}/tmac/hdtbl.tmac
%{_datadir}/%{name}/%{version}/tmac/lbp.tmac
%{_datadir}/%{name}/%{version}/tmac/lj4.tmac
%{_datadir}/%{name}/%{version}/tmac/m.tmac
%{_datadir}/%{name}/%{version}/tmac/me.tmac
%{_datadir}/%{name}/%{version}/tmac/mm.tmac
%{_datadir}/%{name}/%{version}/tmac/mm/
%{_datadir}/%{name}/%{version}/tmac/mmse.tmac
%{_datadir}/%{name}/%{version}/tmac/mom.tmac
%{_datadir}/%{name}/%{version}/tmac/ms.tmac
%{_datadir}/%{name}/%{version}/tmac/mse.tmac
%{_datadir}/%{name}/%{version}/tmac/om.tmac
%{_datadir}/%{name}/%{version}/tmac/pdf.tmac
%{_datadir}/%{name}/%{version}/tmac/pdfmark.tmac
%{_datadir}/%{name}/%{version}/tmac/refer-me.tmac
%{_datadir}/%{name}/%{version}/tmac/refer-mm.tmac
%{_datadir}/%{name}/%{version}/tmac/refer-ms.tmac
%{_datadir}/%{name}/%{version}/tmac/refer.tmac
%{_datadir}/%{name}/%{version}/tmac/s.tmac
%{_datadir}/%{name}/%{version}/tmac/spdf.tmac
%{_datadir}/%{name}/%{version}/tmac/trace.tmac
%{_datadir}/%{name}/%{version}/tmac/zh.tmac
%{_bindir}/addftinfo
%{_bindir}/eqn2graph
%{_bindir}/gdiffmk
%{_bindir}/grap2graph
%{_bindir}/grn
%{_bindir}/grodvi
%{_bindir}/grolbp
%{_bindir}/grolj4
%{_bindir}/hpftodit
%{_bindir}/indxbib
%{_bindir}/lkbib
%{_bindir}/lookbib
%{_bindir}/pdfroff
%{_bindir}/pfbtops
%{_bindir}/pic2graph
%{_bindir}/refer
%{_bindir}/tfmtodit
%{_bindir}/grefer
%{_bindir}/glookbib
%{_bindir}/gindxbib
%{_bindir}/afmtodit
%{_bindir}/chem
%{_bindir}/gperl
%{_bindir}/gpinyin
%{_bindir}/glilypond
%{_bindir}/groffer
%{_bindir}/grog
%{_bindir}/gropdf
%{_bindir}/mmroff
%{_bindir}/pdfmom
%{_bindir}/roff2dvi
%{_bindir}/roff2html
%{_bindir}/roff2pdf
%{_bindir}/roff2ps
%{_bindir}/roff2text
%{_bindir}/roff2x

%files base
%license COPYING FDL LICENSES
%dir %{_datadir}/%{name}/
%dir %{_datadir}/%{name}/%{version}/
%dir %{_datadir}/%{name}/%{version}/font/
%dir %{_datadir}/%{name}/%{version}/tmac/
%{_datadir}/%{name}/current
%{_datadir}/%{name}/%{version}/eign
%{_datadir}/%{name}/%{version}/font/devascii/
%{_datadir}/%{name}/%{version}/font/devlatin1/
%{_datadir}/%{name}/%{version}/font/devps/
%{_datadir}/%{name}/%{version}/font/devutf8/
%{_datadir}/%{name}/%{version}/font/devhtml/
%{_datadir}/%{name}/%{version}/tmac/an-ext.tmac
%{_datadir}/%{name}/%{version}/tmac/an-old.tmac
%{_datadir}/%{name}/%{version}/tmac/an.tmac
%{_datadir}/%{name}/%{version}/tmac/andoc.tmac
%{_datadir}/%{name}/%{version}/tmac/composite.tmac
%{_datadir}/%{name}/%{version}/tmac/cp1047.tmac
%{_datadir}/%{name}/%{version}/tmac/cs.tmac
%{_datadir}/%{name}/%{version}/tmac/de.tmac
%{_datadir}/%{name}/%{version}/tmac/den.tmac
%{_datadir}/%{name}/%{version}/tmac/devtag.tmac
%{_datadir}/%{name}/%{version}/tmac/doc-old.tmac
%{_datadir}/%{name}/%{version}/tmac/doc.tmac
%{_datadir}/%{name}/%{version}/tmac/eqnrc
%{_datadir}/%{name}/%{version}/tmac/europs.tmac
%{_datadir}/%{name}/%{version}/tmac/fallbacks.tmac
%{_datadir}/%{name}/%{version}/tmac/fr.tmac
%{_datadir}/%{name}/%{version}/tmac/html-end.tmac
%{_datadir}/%{name}/%{version}/tmac/html.tmac
%{_datadir}/%{name}/%{version}/tmac/hyphen.cs
%{_datadir}/%{name}/%{version}/tmac/hyphen.den
%{_datadir}/%{name}/%{version}/tmac/hyphen.det
%{_datadir}/%{name}/%{version}/tmac/hyphen.fr
%{_datadir}/%{name}/%{version}/tmac/hyphen.sv
%{_datadir}/%{name}/%{version}/tmac/hyphen.us
%{_datadir}/%{name}/%{version}/tmac/hyphenex.cs
%{_datadir}/%{name}/%{version}/tmac/hyphenex.us
%{_datadir}/%{name}/%{version}/tmac/ja.tmac
%{_datadir}/%{name}/%{version}/tmac/latin1.tmac
%{_datadir}/%{name}/%{version}/tmac/latin2.tmac
%{_datadir}/%{name}/%{version}/tmac/latin5.tmac
%{_datadir}/%{name}/%{version}/tmac/latin9.tmac
%{_datadir}/%{name}/%{version}/tmac/man.tmac
%{_datadir}/%{name}/%{version}/tmac/mandoc.tmac
%{_datadir}/%{name}/%{version}/tmac/mdoc.tmac
%{_datadir}/%{name}/%{version}/tmac/mdoc/
%{_datadir}/%{name}/%{version}/tmac/papersize.tmac
%{_datadir}/%{name}/%{version}/tmac/pdfpic.tmac
%{_datadir}/%{name}/%{version}/tmac/pic.tmac
%{_datadir}/%{name}/%{version}/tmac/ps.tmac
%{_datadir}/%{name}/%{version}/tmac/psatk.tmac
%{_datadir}/%{name}/%{version}/tmac/psold.tmac
%{_datadir}/%{name}/%{version}/tmac/pspic.tmac
%{_datadir}/%{name}/%{version}/tmac/safer.tmac
%{_datadir}/%{name}/%{version}/tmac/sv.tmac
%{_datadir}/%{name}/%{version}/tmac/trans.tmac
%{_datadir}/%{name}/%{version}/tmac/troffrc
%{_datadir}/%{name}/%{version}/tmac/troffrc-end
%{_datadir}/%{name}/%{version}/tmac/tty-char.tmac
%{_datadir}/%{name}/%{version}/tmac/tty.tmac
%{_datadir}/%{name}/%{version}/tmac/unicode.tmac
%{_datadir}/%{name}/%{version}/tmac/www.tmac
%{_datadir}/%{name}/site-tmac/*
%{_bindir}/eqn
%{_bindir}/groff
%{_bindir}/grops
%{_bindir}/grotty
%{_bindir}/neqn
%{_bindir}/nroff
%{_bindir}/pic
%{_bindir}/post-grohtml
%{_bindir}/pre-grohtml
%{_bindir}/preconv
%{_bindir}/soelim
%{_bindir}/tbl
%{_bindir}/troff
%{_bindir}/gnroff
%{_bindir}/gtroff
%{_bindir}/gtbl
%{_bindir}/gpic
%{_bindir}/geqn
%{_bindir}/gneqn
%{_bindir}/gsoelim
%{_bindir}/zsoelim
%{_libdir}/groff/

%if %{with_x}
%files x11
# data
%{_datadir}/%{name}/%{version}/font/devX*/
%{_datadir}/%{name}/%{version}/tmac/X.tmac
%{_datadir}/%{name}/%{version}/tmac/Xps.tmac
%{_datadir}/X11/app-defaults/GXditview
%{_datadir}/X11/app-defaults/GXditview-color
# programs
%{_bindir}/gxditview
%{_bindir}/xtotroff
%endif

%files help
%defattr(-,root,root)
%{_mandir}/man1/
%{_mandir}/man5/
%{_mandir}/man7/
%doc %{_docdir}/%{name}-%{version}/
%exclude %{_docdir}/%{name}-%{version}/examples/mom/slide-demo.pdf
%doc BUG-REPORT MORE.STUFF NEWS PROBLEMS
%{_infodir}/groff.info*

%changelog
* Tue Oct 25 2022 yanglongkang <yanglongkang@h-partners.com> - 1.22.4-10
- rebuild for next release

* Tue Mar 15 2022 renhognxun <renhongxun@h-partners.com> 1.22.4-9
- split a sub-package groff-x11

* Wed Sep 01 2021 Jianmin <jianmin@iscas.ac.cn> - 1.22.4-8
- move basic files into base package.

* Tue Aug 17 2021 Jianmin <jianmin@iscas.ac.cn> - 1.22.4-7
- split groff-base from groff

* Thu Jul 22 2021 wuchaochao <wuchaochao4@huawei.com> - 1.22.4-5
- Remove BuildRequires gdb

* Sat Mar 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.22.4-4
- fixbug in self-building

* Wed Jan  8 2020 JeanLeo <liujianliu.liu@huawei.com> - 1.22.4-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:update software package

* Fri Oct 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.22.4-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:add the need buildrequires

* Fri Sep 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.22.4-1
- Package init
